#!/bin/bash

# Apply plugin updates for a list of sites,
# and deploy them to the Test environment.
#
# Usage: ./wp-plugin-updates.sh SITES

set -eou pipefail

UPDATED=()
FAILED_GIT=()

# Loop through all sites on the list.
for SITENAME in ${SITES//,/ }; do
  echo "Site: $SITENAME"
  # Check for plugin updates.
  PLUGINS="$(terminus wp "${SITENAME}".dev -- plugin update --all --format=json --dry-run </dev/null | jq -c '.[]')"

  if [ -n "$PLUGINS" ]; then
    echo "$SITENAME has updates."
    terminus connection:set "${SITENAME}".dev git

    # Clone the site to a temporary directory.
    TMP_DIR=$(mktemp -d 2>/dev/null || mktemp -d -t)

    GIT_URL="$(terminus connection:info "${SITENAME}".dev --field=git_url)"
    git clone "${GIT_URL}" "${TMP_DIR}"/"${SITENAME}"
    cd "${TMP_DIR}"/"${SITENAME}"

    for PLUGIN in $PLUGINS; do
      SLUG="$(echo "$PLUGIN" | jq -r '.name')"
      INSTALLED="$(echo "$PLUGIN" | jq -r '.version')"
      AVAILABLE="$(echo "$PLUGIN" | jq -r '.update_version')"
      MESSAGE="Update $SLUG ($INSTALLED => $AVAILABLE)."

      curl -O https://downloads.wordpress.org/plugin/"${SLUG}".zip
      FILE="$SLUG".zip

      if file "$FILE" | grep -iq "Zip archive"; then
        rm -rf wp-content/plugins/"${SLUG}"
        unzip "$FILE" -d wp-content/plugins/
        rm "$FILE"
        git add . && git commit -am "${MESSAGE}"
        UPDATED+=("${SITENAME}")
      else
        # If Git mode failed, try updating the plugin with 
        # SFTP mode and WP-CLI. This can be neccessary for 
        # plugins that need a license key to fetch the update.
        terminus connection:set "${SITENAME}".dev sftp
        terminus wp "${SITENAME}".dev -- plugin update "${SLUG}"
        sleep 5 # Let Pantheon SFTP mode catch up
        terminus env:commit "${SITENAME}".dev --message="${MESSAGE}"
        sleep 5 # Let Pantheon SFTP mode catch up
        terminus connection:set "${SITENAME}".dev git -y
        git pull --rebase
        FAILED_GIT+=("${SITENAME} ${SLUG}")
      fi
    done

    # Push to Pantheon dev.
    git push origin master

    # Apply DB updates on dev all at once.
    terminus wp "${SITENAME}".dev -- core update-db </dev/null

    # Deploy to Test env.
    terminus env:deploy --sync-content --note="Plugin updates." -- "${SITENAME}".test
    terminus wp "${SITENAME}".test -- core update-db </dev/null
    terminus env:clear-cache "${SITENAME}".test
  fi
done

# Holler about the fails.
if ((${#FAILED_GIT[@]})); then
  echo "[warning] Some updates failed to apply with Git and were applied via SFTP:"
  for FAIL in "${FAILED_GIT[@]}"; do
    echo -e "${FAIL}"
  done
fi

# Updates are done, go check them out.
# @todo de-dupe "updated" (it's counting each plugin vs each site)
if ((${#UPDATED[@]})); then
  echo "[notice] Go check the Test sites: "
  for SITENAME in "${UPDATED[@]}"; do
    echo -e "- https://test-${SITENAME}.pantheonsite.io"
  done
fi